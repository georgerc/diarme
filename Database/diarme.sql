-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mai 2017 la 22:17
-- Versiune server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diarme`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `id_of_post` int(11) NOT NULL,
  `message` text NOT NULL,
  `username` text NOT NULL,
  `odata` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `journals`
--

CREATE TABLE `journals` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `journal_title` text NOT NULL,
  `journal_text` text NOT NULL,
  `odata` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `share` int(1) DEFAULT '0',
  `img_path` text NOT NULL,
  `warned` int(11) NOT NULL,
  `warned_reason` text NOT NULL,
  `longitude` float NOT NULL DEFAULT '1',
  `latitude` float NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  `old_pass` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` int(1) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL,
  `suspended` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `members`
--

INSERT INTO `members` (`id`, `username`, `password`, `old_pass`, `email`, `reg_time`, `avatar`, `admin`, `suspended`) VALUES
(62, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'ee11cbb19052e40b07aac0ca060c23', 'user@user.com', '2017-05-09 20:17:29', 0, 0, 0),
(61, 'admin', '21232f297a57a5a743894a0e4a801fc3', '21232f297a57a5a743894a0e4a801f', 'admin@admin.com', '2017-05-09 20:17:16', 0, 1, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `id_of_comment` int(11) NOT NULL,
  `upvote` int(11) NOT NULL,
  `id_of_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;